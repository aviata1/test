module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/style/base/variables.scss";`
      }
    }
  }
};
