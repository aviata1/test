import Vue from "vue";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

import dateTime from "./filters/dateTime";
import currency from "./filters/currency";
Vue.filter("dateTime", dateTime);
Vue.filter("currency", currency);

new Vue({
  router,
  render: (h) => h(App)
}).$mount("#app");
