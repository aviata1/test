export default function(value, currency = "KZT") {
  const symbols = {
    kzt: "₸",
    usd: "$",
    rub: "₽"
  };

  return `${value} ${symbols[currency.toLowerCase()]}`;
}
